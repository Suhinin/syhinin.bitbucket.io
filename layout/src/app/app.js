
export default {
  name: 'app',
  data () {
    return {
      msg: 'Welcome to Your Vue.js App',
      tasks: [
        {title: 'name1', done: true, isEding: false },
        {title: 'name2', done: false, isEding: false }
      ],
      newTask: '',
      editThis: '',
      filterTask: null
    }
  },
  computed: {
    filteredTasks() {
      const status = this.filterTask;
      return this.tasks.filter(function(item){
        return status === null || item.done === status;
      });
    }
  },
  methods: {
    addTask : function (e) {
      e.preventDefault();
     this.tasks.push({ title: this.newTask, done: false, isEding: false });
     this.newTask = '';
    },
    removeTask : function (index){
        this.tasks.splice(index, 1);

    },
    editTask : function(index) {
      this.$refs.editThisTask[index].autofocus = true;
      this.tasks[index].isEding = true;
    },
    editChange : function(index) {
      var itemArr = this.tasks[index];
      itemArr.title = this.editThis;
      itemArr.isEding = false;
      this.editThis = '';

       var inputsArr = this.$refs.editThisTask;
       for(var i = 0; i < inputsArr.length; i++){
         inputsArr.autofocus = false;
       }
    },
    statusDone : function(index){
      this.tasks[index].done = true;
    },
    filter : function (status) {
      this.filterTask = status;
    }
  },

}


